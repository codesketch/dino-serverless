# THIS PROJECT HAS BEEN DISCONTINUED AND THE CODEBASE MERGED INTO [DINO-EXPRESS](https://gitlab.com/codesketch/dino-express/) WHERE IT WILL BE MAINTAINED.
# THIS PROJECT WILL NOT RECEIVE ANY MORE UPDATES, PLEASE UPDATE TO DINO-EXPRESS:1.0.5 OR HIGHER TO RECEIVE UPDATES.
# THE PORTING IS BACKWARD COMPATIBLE APART FOR THE REQUIRED ENTRYPOINT WHICH SHOULD BE IMPLEMENTED AS BELOW:
```
import { ObjectHelper } from 'dino-core'
import { Runner } from 'dino-express'
import { readFileSync } from 'fs'

if (ObjectHelper.isNotDefined(process.env.DINO_CONTEXT_ROOT)) {
  process.env.DINO_CONTEXT_ROOT = 'dist/interfaces,dist/infrastructure,dist/application'
}
if (ObjectHelper.isNotDefined(process.env.DINO_CONFIG_PATH)) {
  process.env.DINO_CONFIG_PATH = 'src/main/resources/dev.config.json'
}

const runner = new Runner()
if (process.env.DINO_CONFIG_PATH !== undefined) {
  const config = JSON.parse(readFileSync(process.env.DINO_CONFIG_PATH, 'utf8'))
  if (['express', 'standalone'].includes(config.dino.server.type)) {
    void runner.load()
  }
}
exports.handler = runner.run.bind(runner)

``` 

## DinO Serverless

DinO Serverless's aim is to create a configuration first framework for exposing web APIs in a serverless architectural configuration. The basic idea is that the first step of development starts with defining your Swagger/OpenAPI definition and provide it to DinO Serverless which will map your API and connect directly your code using the standard operationId defined for each API, this allow you to focus on your business requirements and the aspects that matters for your application leaving the boilerplate to DinO Serverless.

In order to be able to connect your Swagger/OpenAPI definition with your code seemlesly DinO Serverless define the concept of Interface, an interface is a Component on [DinO](https://gitlab.com/codesketch/dino) world which responsibility is to accept the input from the users and propagate, after the required checks are performed, it to your services.

An example of how DinO Serverless works is provided on the **examples/json** folder, you can run it using the following command:

```bash
DINO_CONTEXT_ROOT=examples/json DINO_CONFIG_PATH=examples/json/config.json node examples/json/main.js
```

DinO Serverless integrate with Swagger UI, this allow you to easilly test your APIs and provide a clear interface for documentation, the Swagger UI will ba accessible at http://localhost:3030/api-docs or in general http[s]://hostname[:port]/api-docs.

## Configuration

API configuration is primarily done via OpenAPI 3.0 definition. Some aspects are configured via a configuration file.

### Monitoring

If enabled an additional endpoint `/monitor` is exposed that provides usefull information about the application status.

Exposed information are:

1. UP/DOWN status
2. Per-API cache - when enabled - information
3. CO2eq runtime emissions if enabled via the `dino.server.monitoring.co2` configuration. CO2eq monitoring is executed using the [CO2.js library provided by The Greeen Web Foundation](https://developers.thegreenwebfoundation.org/co2js/overview/)

Monitoring is enabled by default and can be controlled enabled/disabled using the `dino.server.monitoring.enabled` configuration.

#### Example

```json
{
  "dino": {
    "monitoring": {
      "enabled": true|false,
      "c02": true|false
    }
  }
}
```

### Caching

Dino-express uses [apicache-plus](https://www.npmjs.com/package/apicache-plus) module to provide caching functionalities. Caching is enabled adding the `cache` directive as part of the API definition, available configuration aspects are:

| Parameter        | Description                                                                                                   | Default    |
| ---------------- | ------------------------------------------------------------------------------------------------------------- | ---------- |
| ttl              | The TTL of the cache for this API                                                                             | 30 minutes |
| debug            | Enable debug statements for on console                                                                        | false      |
| trackPerformance | Enable performance tracking for the API cache, performance monitoring can be observed via `/monitor` endpoint | true       |
| isBypassable     | Allows to bypass cache by requesting with Cache-Control: no-store header                                      | true       |

#### Example

```yaml
---
paths:
  /uuid:
    get:
      summary: Get a UUID
      operationId: getUUID
      tags:
        - uuid
      parameters: {}
      cache:
        ttl: 30 Seconds
        debug: false
        trackPerformance: true
        isBypassable: true
      responses:
        '200':
          description: An object containing a UUID
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UUID'
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
```

## Contributing

Given the scarce amount of time in my hands the pace of the project is really slow, so contribution are welcome.

If you are interested in contributing to the project please do not hesitate have a look at the [contributing](./CONTRIBUTING.md) guidelines or get in touch.
