export const ServerlessConfiguration: typeof import("./infrastructure/serverless.configuration");
export const RoutingConfigurerServerless: typeof import("./infrastructure/routing.configurer.serverless");
export const Serverless: typeof import("./infrastructure/serverless");
export const Response: typeof import("./infrastructure/response");
//# sourceMappingURL=index.d.ts.map