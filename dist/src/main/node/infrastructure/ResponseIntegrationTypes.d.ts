export namespace ResponseIntegrationType {
    const PROXY: string;
    const METHOD: string;
    function isProxyType(type: any): boolean;
    function fromName(name: any): "proxy" | "method";
}
//# sourceMappingURL=ResponseIntegrationTypes.d.ts.map