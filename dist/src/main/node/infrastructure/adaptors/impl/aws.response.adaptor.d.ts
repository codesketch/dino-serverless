export = AwsResponseAdaptor;
declare class AwsResponseAdaptor {
    constructor(environment: any);
    serverType: any;
    responseAsBase64: any;
    integrationType: string;
    adapt(serviceResponse: any, callback: any, isError: any): any;
    buildAWSResponse(response: any): {
        statusCode: any;
        headers: any;
        body: any;
        isBase64Encoded: any;
    };
    buildAWSProxyResponse(response: any): {
        statusCode: any;
        headers: any;
        multiValueHeaders: {};
        body: any;
        isBase64Encoded: any;
    };
}
//# sourceMappingURL=aws.response.adaptor.d.ts.map