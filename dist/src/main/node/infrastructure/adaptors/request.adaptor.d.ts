export = RequestAdaptor;
declare class RequestAdaptor {
    /**
     *
     * @param {EnvironmentConfiguration} environment the current environment configuration
     */
    constructor(environment: EnvironmentConfiguration);
    requestBodyParser: ServerlessRequestBodyParser;
    adapt(message: any, exposedAs: any): Request;
    getMethod(message: any, exposedAs: any): any;
    /**
     * Build the request URL if required
     * @param {Object} message the request message
     * @returns {String}
     *
     * @private
     */
    private buildUrl;
    getOriginalUrl(message: any): any;
    setTransferEncodingIfRequired(message: any): void;
    headersToLowerCase(message: any): void;
    isApi(exposedAs: any): boolean;
}
declare const ServerlessRequestBodyParser_base: typeof import("dino-express/dist/request/request.body.parser");
/**
 * Extends request body parser to properly extract the request body
 */
declare class ServerlessRequestBodyParser extends ServerlessRequestBodyParser_base {
    extractBody(message: any): any;
}
import Request = require("../request");
import { EnvironmentConfiguration } from "dino-core";
//# sourceMappingURL=request.adaptor.d.ts.map