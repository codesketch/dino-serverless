export = ApiValidator;
declare class ApiValidator {
    constructor(environment: any);
    environment: any;
    validationEnabled: any;
    init(apis: any): void;
    openApiValidator: OpenAPIValidator;
    getRequestValidator(method: any, path: any): (req: any, res: any, next: any) => any;
    getResponseValidator(method: any, path: any): (_req: any, res: any, next: any) => void;
}
import OpenAPIValidator_1 = require("express-openapi-validate/dist/OpenApiValidator");
import OpenAPIValidator = OpenAPIValidator_1.OpenApiValidator;
//# sourceMappingURL=api.validator.d.ts.map