export = Cache;
declare class Cache {
    constructor(environment: any);
    environment: any;
    map: Map<any, any>;
    maxAge: any;
    maxItems: any;
    forceEvictionOnMaxItems: any;
    get(key: any): any;
    set(key: any, value: any): any;
    evict(key: any): void;
    __forceEvictionIfRequired(forceEviction: any): void;
}
//# sourceMappingURL=cache.d.ts.map