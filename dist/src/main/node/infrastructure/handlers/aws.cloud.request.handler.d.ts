export = AwsCloudRequestHandler;
declare class AwsCloudRequestHandler extends RequestHandler {
    /**
     * Collect the parameters defined as part of the OpenAPI definition.
     * @param {Object} message the express request
     */
    _collectParameters(message: any): any;
    supports(): string;
}
import RequestHandler = require("./request.handler");
//# sourceMappingURL=aws.cloud.request.handler.d.ts.map