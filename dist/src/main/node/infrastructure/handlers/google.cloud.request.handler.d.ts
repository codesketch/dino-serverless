export = GoogleCloudRequestHandler;
declare class GoogleCloudRequestHandler extends RequestHandler {
    /**
     * Collect the parameters defined as part of the OpenAPI definition.
     * @param {Object} message the express request
     */
    _collectParameters(message: any): any;
    supports(): string;
}
import RequestHandler = require("./request.handler");
//# sourceMappingURL=google.cloud.request.handler.d.ts.map