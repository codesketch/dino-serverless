export = RequestHandler;
declare class RequestHandler extends Component {
    static scope(): Scope;
    constructor({ applicationContext, environment }: {
        applicationContext: any;
        environment: any;
    });
    environment: any;
    applicationContext: any;
    _handler: any;
    apiHandlingType: any;
    handle(request: any, response: any, next: any): Promise<void>;
    /**
     * @returns Promise<Response|unknown>
     */
    handleRequest(message: any, handler: any): Promise<any>;
    addApi(api: any): RequestHandler;
    api: any;
    addResponseValidator(responseValidator: any): RequestHandler;
    responseValidator: any;
    _collectParameters(message: any): any;
    /**
     * Lookup the interface that can handle the defined operationId for the API.
     * @returns {Interface}
     *
     * @private
     */
    private ensureHandler;
    normaliseSuccessResponse(apiResponse: any, response: any): void;
    normaliseErrorResponse(error: any, response: any): void;
    buildClientErrorResponse(properties: any, contentType: any, error: any): string;
}
import { Component } from "dino-core";
import { Scope } from "dino-core";
//# sourceMappingURL=request.handler.d.ts.map