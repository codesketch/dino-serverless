export = Helper;
declare class Helper {
    /**
     * Normalize swagger path definition to express notation
     * @param {String} path the API path
     *
     * @public
     * @static
     */
    public static normalizePath(path: string): string;
    /**
     * Allows to retrieve a variable from the configuration or as environment variable, supports
     * both `:` and `_` notation.
     * @param {Environment} environment the configuration container
     * @param {string} key the key for the varible to retrieve
     * @param {any} _default the default value if the variable is nt defined.
     */
    static getVariable(environment: Environment, key: string, _default: any): any;
    /**
     * Ensure that either the value or default value are returned.
     * @param {any} value the value to verify
     * @param {any} the default value to return
     */
    static ensureValue(value: any, _default: any): any;
    static requestHasBody(req: any): boolean;
    /**
     * Format the response based on the content type defined
     * @returns {String}
     */
    static format(payload: any, contentType: any): string;
    static isAPromise(obj: any): boolean;
    /**
     * Evaluate if an obj is an instance of a type
     *
     * @param {Any} obj the object to be verified
     * @param {Any} type the type the object will be verified against
     * @returns {Boolean} true if obj is instance of type, fasle otherwise
     */
    static instanceOf(obj: Any, type: Any): boolean;
    static base64RegExp: RegExp;
    static isBase64(v: any): boolean;
    static decodeFromBase64AndParse(str: any): any;
    /**
     * Normalise the response body from the received error
     * @param {any} response the response for the client
     * @returns the body response
     *
     * @private
     */
    private static buildResponseBody;
}
//# sourceMappingURL=helper.d.ts.map