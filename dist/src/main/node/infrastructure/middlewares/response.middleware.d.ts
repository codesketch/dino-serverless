export = ResponseMiddleware;
declare class ResponseMiddleware {
    /**
   * @param {Request} req
   * @param {Response} res
   */
    static handler(req: Request, res: Response): void;
}
//# sourceMappingURL=response.middleware.d.ts.map