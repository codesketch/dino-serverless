export = ServerlessApplicationStateMonitor;
declare class ServerlessApplicationStateMonitor {
    doMonitor(res: any): import("dino-express/dist/response");
    middleware(): (req: any, response: any, next: any) => void;
}
//# sourceMappingURL=application.state.monitor.d.ts.map