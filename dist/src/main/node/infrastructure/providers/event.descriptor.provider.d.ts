export = EventDescriptorProvider;
declare class EventDescriptorProvider {
    /**
     * Provide a reference to an OpenAPI descriptor stored on the local file-system, the location
     * of the descriptor can be provided using any of the `dino:openapi:descriptor`,
     * `dino_openapi_descriptor` or `DINO_OPENAPI_DESCRIPTOR` configuration variables,
     * if the configuration is not provided the default `src/main/resources/routes.yml` path will
     * be used.
     * @returns {string | object} the path of the OpenAPI descriptor file or a default events descriptor.
     */
    provide(): string | object;
}
//# sourceMappingURL=event.descriptor.provider.d.ts.map