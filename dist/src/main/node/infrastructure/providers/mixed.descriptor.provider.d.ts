export = MixedDescriptorProvider;
declare class MixedDescriptorProvider {
    constructor(environment: any);
    environment: any;
    /**
     * Provide a reference to an OpenAPI descriptor stored on the local file-system, the location
     * of the descriptor can be provided using any of the `dino:openapi:descriptor`,
     * `dino_openapi_descriptor` or `DINO_OPENAPI_DESCRIPTOR` configuration variables,
     * if the configuration is not provided the default `src/main/resources/routes.yml` path will
     * be used.
     * @returns {string | object} the path of the OpenAPI descriptor file or a default events descriptor.
     */
    provide(): string | object;
    getDefaultEventDescriptor(): {
        provide: () => {
            openapi: string;
            info: {
                version: string;
                title: string;
                license: {
                    name: string;
                };
                description: string;
            };
            servers: {
                url: string;
            }[];
            paths: {
                '/': {
                    post: {
                        summary: string;
                        description: string;
                        operationId: string;
                        tags: string[];
                        parameters: any[];
                        requestBody: {
                            description: string;
                            content: {
                                'application/json': {
                                    schema: {
                                        $ref: string;
                                    };
                                };
                            };
                        };
                        responses: {
                            201: {
                                description: string;
                                content: {
                                    'application/json': {
                                        schema: {
                                            $ref: string;
                                        };
                                    };
                                };
                            };
                            default: {
                                description: string;
                                content: {
                                    'application/json': {
                                        schema: {
                                            $ref: string;
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
            components: {
                schemas: {
                    Event: {
                        type: string;
                        additionalProperties: boolean;
                    };
                    GenericResponse: {
                        type: string;
                        additionalProperties: boolean;
                    };
                    Error: {
                        type: string;
                        additionalProperties: boolean;
                    };
                };
            };
        };
    };
}
//# sourceMappingURL=mixed.descriptor.provider.d.ts.map