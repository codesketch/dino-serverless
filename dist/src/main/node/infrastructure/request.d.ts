export = Request;
declare class Request {
    static create(method: any, url: any): Request;
    constructor(method: any, url: any);
    url: any;
    method: any;
    body: any;
    query: queryString.ParsedQuery<string>;
    params: {};
    headers: {};
    requestPath: any;
    path(exposedAs: any): any;
    getMethod(exposedAs: any): any;
    setPath(path: any): Request;
    setHeaders(headers?: {}): Request;
    setBody(body: any): Request;
    resolveParams(keys: any, match: any): void;
    isGet(): boolean;
    __decodeParam(val: any): any;
}
import queryString = require("query-string");
//# sourceMappingURL=request.d.ts.map