export = Response;
declare const Response_base: typeof import("dino-express/dist/response");
declare class Response extends Response_base {
    context: {};
    error: boolean;
    defineContext(onSuccess: any, onFailure: any, responseAdaptor: any, serverResponse: any): Response;
    defineAsErrorResponse(): Response;
    getContext(): {};
    isErrorResponse(): boolean;
    setPayload(payload: any): Response;
    setStatusCode(statusCode: any): Response;
    status(statusCode: any): Response;
    send(body: any): Response;
    getStatusCode(): number;
    getHeaders(): {
        'X-Powerd-By': string;
    };
    getBody(): any;
    format(obj: any): Response;
    getAdaptor(obj: any): any;
}
//# sourceMappingURL=response.d.ts.map