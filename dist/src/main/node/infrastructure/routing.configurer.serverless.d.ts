export = RoutingConfigurerServerless;
declare const RoutingConfigurerServerless_base: typeof import("dino-express/dist/routing.configurer");
declare class RoutingConfigurerServerless extends RoutingConfigurerServerless_base {
    constructor({ applicationContext, environment }: {
        applicationContext: any;
        environment: any;
    });
    requestHandlerFactory: RequestHandlerFactory;
    setupMonitoringEndpointsIfRequired(router: any): void;
    requestHandler(api: any, responseValidator: any, cloudProvider: any): any;
    errorHandler(cloudProvider: any): (err: any, req: any, res: any, next: any) => void;
}
import RequestHandlerFactory = require("./request.handler.factory");
//# sourceMappingURL=routing.configurer.serverless.d.ts.map