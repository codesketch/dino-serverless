export = Api;
declare class Api {
    static create(method: any, path: any, operationId: any, middlewares: any, responseValidator: any): Api;
    constructor(method: any, path: any, operationId: any, middlewares: any, responseValidator: any);
    keys: any[];
    path: any;
    method: any;
    operationId: any;
    middlewares: any[];
    errorHandlers: any[];
    responseValidator: any;
    regexp: RegExp;
    /**
     * Check if this route matches `path`, if so
     * populate `.params`.
     *
     * @param {String} path
     * @return {Boolean}
     * @api public
     */
    matches(currentPath: any): boolean;
    pathMatch: RegExpExecArray;
    validateResponse(response: any): boolean;
    /**
     * @param {Request} req
     * @param {Response} res
     */
    process(request: any, response: any, cloudProvider: any): void;
    _selectMiddlewares(middlewares: any): void;
}
//# sourceMappingURL=api.d.ts.map