export = Router;
declare class Router {
    apis: Map<any, any>;
    middlewares: any[];
    use(middleware: any): void;
    addApi(method: any, path: any, operationId: any, middlewares: any, responseValidator: any): void;
    /**
     * Get the handling API definition
     * @param {String} method the HTTP API method
     * @param {String} path the current invoke path
     *
     * @public
     */
    public getApi(method: string, path: string): any;
}
//# sourceMappingURL=router.d.ts.map