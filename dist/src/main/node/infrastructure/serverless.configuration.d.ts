export = ServerlessConfiguration;
declare class ServerlessConfiguration extends Configuration {
    configure(): import("dino-core").ComponentDescriptor;
}
import { Configuration } from "dino-core";
//# sourceMappingURL=serverless.configuration.d.ts.map