export = Serverless;
declare class Serverless {
    static getInstance(): any;
    router: any;
    applicationContext: import("dino-core").ApplicationContext;
    requestAdaptor: RequestAdaptor;
    responseAdaptor: ResponseAdaptor;
    loadContext(config: any): Promise<Serverless>;
    environment: any;
    serverType: any;
    cloudProvider: any;
    exposedAs: any;
    intervalTimer: NodeJS.Timeout;
    handler(request: any, response: any, reject: any): any;
    expose(request: any, response: any): Promise<any>;
    __discoverRouter(): void;
    __buildErrorResponse(payload: any): any;
    __sendResponse(currentResponse: any, response: any, callback: any, isError?: boolean): void;
}
import RequestAdaptor = require("./adaptors/request.adaptor");
import ResponseAdaptor = require("./adaptors/response.adaptor");
//# sourceMappingURL=serverless.d.ts.map