/* eslint-disable no-unused-vars */

//    Copyright 2020 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global process */
/* global setTimeout */
/* global console */

process.env.DINO_CONTEXT_ROOT = 'examples/src/main/node'
process.env.DINO_CONFIG_PATH = 'examples/src/main/resources/config.json'

const Serverless = require('../../../src/main/node/index').Serverless

class Response {
  status(sc) {
    this.sc = sc
    return this
  }
  type(ct) {
    this.ct = ct
    return this
  }
  set(n, v) {
    console.log(`Headers >> ${n}, ${v}`)
    return this
  }
  send(data) {
    this.data = data
    console.log(`Output >> ${JSON.stringify(data, null, 2)}`)
    return this
  }
  format(formatters) {
    if (this.ct === 'application/json') {
      formatters.json()
    } else {
      formatters.text()
    }
    return this
  }

  propagate(res) {
    console.log(`Output >> ${JSON.stringify(res, null, 2)}`)
  }
}
const response = new Response()
const serverless = new Serverless()
serverless.loadContext()
const fcn = serverless.expose.bind(serverless)
// This is not required on for the integration is here only to showcase the example
fcn(
  {
    body: {
      Records: [{ 'body': 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==' }]
    },
    // path: '/statistics/75b84647-d294-4856-8c79-1218568dd4e9',
    method: 'POST',
    isBase64Encoded: false,
    params: {
      accountId: '75b84647-d294-4856-8c79-1218568dd4e9'
    },
    query: {},
    header: {
      accept: 'application/json',
      'accept-encoding': 'gzip, deflate, br',
      'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
      Host: 'uacm51ieaa.execute-api.eu-west-2.amazonaws.com',
      origin: 'http://localhost:3000',
      referer: 'http://localhost:3000/',
      'sec-fetch-dest': 'empty',
      'sec-fetch-mode': 'cors',
      'sec-fetch-site': 'cross-site',
      'User-Agent':
        'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36',
      'x-amz-date': '20230323T201842Z',
      'x-amz-security-token':
        'IQoJb3JpZ2luX2VjEP3//////////wEaCWV1LXdlc3QtMiJGMEQCIEyhZlkVNPc7jkGOZ9DO7HTsiVS3owzY0dKbH1JmH9wKAiALYNs2cg1Cq1DfBDKAKnWVTcW11u1S21UZ85+42gZ0WCrNBAjF//////////8BEAEaDDk3NTk0NjQ5MzA2NyIMWFVDk1oTXkZXHVktKqEE65QpfVv8X6WUg3ouueydYhrqxIk8bDxIB7V9YB+8nQG+dBSWjBias0T2FECqiKc48DbqKbDkcMYGPiENWpz5dDp73r4WGgIUN/rlGnWVoutxubEIPKiqekaKUEg2wBfaDXfLoScvVlbCZlakBJO1JPi70mnZDqgFzP9LVQhWC0XFno4dUAqu/wgZiPSM0Ut07AUarRlzbC6NSG2ul7Jqp/36SCrnwUsnqEr8r4Vo4oezuJw3BIYlXX9Ly5FSSmAKu24uYapqdAcmup5sEuwk3+YYe6EXg87qoayXov22xxfh1Mn39ZxO46e/gIMvxA/580zGh2Bmr+FAPfjPe3aSQPJj4ycDQA3QPa5W/AJYll4KjDmlF7h/Hsma3yunzkMKpeB0Q6ZuTlggr0pSgpoat+vmT9yBxY3k48bnC6jpAN7DctfwU9QdA/vL5LXx8FVI6PUa2PEdbhRwgRGA72voe/DPw+cx7RtS0xf3i9pSRB8xv2ME/sMNA1sb7UA4uNM3WaeBoVVfLIfRbuTsvXyQs2/ImKHLWj11VQ5+fE98qHbPQdWourjFMvgZ30l6UC6i0MQKaC4xlSiMSlug3/7OjDXGadI6Qu+hpHkufsaoM9AjJg0ru+2bqEwnYrzeY2KmLTwWmouUb7WQrx1uh7g0XG8BCwLBmEIa0BPe9chdVbZoXglXdKTmXslUY7hKWuBt2QrM/FTXEiNutiJa0yhAK74wn+jyoAY6hgIicqBM0q6/1OJK825ClbryN/HThjxb5xV9sTcjG4mFX4OnOmyyN2UdZ5AXY4abmdEmC8SzhUVYQQNdvPmppzoLlgqrYmmReGiVlxNZ30IcPk3fvXWIM/R7USnoqB6abUb7nrp+vLOtQVg+mmW45UPu353uSRORNE3ysVjHAQ5ssC8L/WlzSSOszVSswGskG+DhBRRn3EJaIUd6VGDF+TxJ5YIV10J0Td0B3eQAG196W42pyLZCdaSrz1HscM0m+Ps0CDpdgpF67CtV+oPTq0CkVwNfMAHy6MYiDqx/CZHAboupr24RloL+ajmX2wBa1okEfdh0iRbR1UZh0T+3nD+hl4dLu8hS',
      'X-Amzn-Trace-Id': 'Root=1-641cb423-4b77183d6854b7754ab22492',
      'X-Forwarded-For': '88.147.62.101',
      'X-Forwarded-Port': '443',
      'X-Forwarded-Proto': 'https',
      'x-ftbd-identity-token':
        'eyJraWQiOiJyNFQyQWxCOU1DVE1MUUJJdE8ya1N1eUdsSHBUKzBUbWpicEZcLzBvV3hERT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIyMTQxYmJhYi04MTY5LTQyYzgtYjA5My0zNDgxMTJjYmI5N2MiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLmV1LXdlc3QtMi5hbWF6b25hd3MuY29tXC9ldS13ZXN0LTJfZzZQN1QxMFRGIiwiY29nbml0bzp1c2VybmFtZSI6IjIxNDFiYmFiLTgxNjktNDJjOC1iMDkzLTM0ODExMmNiYjk3YyIsIm9yaWdpbl9qdGkiOiJjYTU5Njc4OS1hMzYzLTQ3MjctYjc2OC0zNjQ5YThlYTQ0ZjEiLCJhdWQiOiJjZmRoa2ptY2o1aGtncWEzMmZyNzM1NmwxIiwiZXZlbnRfaWQiOiJhZmM1OGIyZi1hZjMyLTQ1NWYtYTIwZC0zZjIyMzgyOWRmYzMiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTY3OTQ2NDIwMywiZXhwIjoxNjc5NjA2MzE0LCJpYXQiOjE2Nzk2MDI3MTUsImp0aSI6ImJhNDc4NjcxLTg5MjMtNGQ1Ni1hN2JkLTI0NGExNTY2ODhjOCIsImVtYWlsIjoicXVpcmluby5icml6aUBnbWFpbC5jb20ifQ.EW_d0DDfyoQ_GNMTx6ZVNXxVCMbBRKgqomUCTqb_CABAiL2LE8vNtRAoAjmOQABTf1VO9_Q9Hl-EgWqZ99WcUauhM-pDQh6Fa3F1CUs0_1KYmXGGSfaV9Yq4chVbmH1wFIRqn1P1-BikOXcn2_iqzNFg4UTW3kbMHSuDXTS1PU9sQurqDJvRGq9wpxSVlnZpTbhovnYUykSJHA_uYReZFZcg3GixnroSvZ49P-UQv4gd_APycsJzyILHxvYsTEqrHTO2ebhLDPh39BmmDALyH1xUF_Cnqq7QPWVn8iCPRnDRRmh2Qmt040EMJzro6GKGYCD-XHoIzx1cLhCwRAVqgQ'
    },
    'stage-variables': {
      ENVIRONMENT: 'development'
    },
    context: {
      'account-id': '',
      'api-id': 'uacm51ieaa',
      'api-key': '',
      'authorizer-principal-id': 'user',
      caller: '',
      'cognito-authentication-provider': '',
      'cognito-authentication-type': '',
      'cognito-identity-id': '',
      'cognito-identity-pool-id': '',
      'http-method': 'GET',
      stage: 'dev',
      'source-ip': '88.147.62.101',
      user: '',
      'user-agent':
        'Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36',
      'user-arn': '',
      'request-id': '9e7aaef0-311f-40a0-90e0-62fc092c3ac0',
      'resource-id': 'ev63ne',
      'resource-path': '/statistics/{accountId}'
    }
  },
  response
)
  .then((res) => {
    // eslint-disable-next-line no-console
    console.log(`Output >> ${JSON.stringify(res, null, 2)}`)
  })
  .catch(console.error)
  .finally(() => {
    fcn({
      body: {},
      path: '/monitor',
      method: 'GET'
    })
      .then((res) => {
        // eslint-disable-next-line no-console
        console.log(`Output >> ${JSON.stringify(res, null, 2)}`)
      })
      .catch(console.error)
  })
