const Interface = require('dino-express/src/main/node/interface')
const { Response, HttpException } = require('dino-express')

class AppInterface extends Interface {
  hello({ g, m, body }) {
    if (m === 'exc') {
      throw new HttpException(500, 'this is my error')
    }
    return Response.builder(200, Object.assign({ [m]: g }, body))
  }

  handle({ body }) {
    if (body.params?.message === 'exc') {
      throw new HttpException(500, 'this is my error')
    }
    return Response.builder(200, (body.Records[0].body ?? body).params)
  }
}

module.exports = AppInterface
