// Copyright 2022 Quirino Brizi [quirino.brizi@gmail.com]
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const { Logger } = require('dino-core')
const Helper = require('../../helper')
const { ResponseIntegrationType } = require('../../ResponseIntegrationTypes')

class AwsResponseAdaptor {
  constructor(environment) {
    this.serverType = Helper.getVariable(environment, 'dino:server:type', 'serverless')
    this.responseAsBase64 = Helper.getVariable(environment, 'dino:server:response:asBase64', false)
    // if there is a typo default to method integration type...
    this.integrationType = ResponseIntegrationType.fromName(Helper.getVariable(environment, 'dino:cloud:integration', 'method'))
  }

  adapt(serviceResponse, callback, isError) {
    const res = ResponseIntegrationType.isProxyType(this.integrationType)
      ? this.buildAWSProxyResponse(serviceResponse)
      : this.buildAWSResponse(serviceResponse)
    Logger.debug(`sending response ${JSON.stringify(res)} built for ${this.integrationType} on ${this.cloudProvider}`)
    return callback(isError ? JSON.stringify(res) : res)
  }

  buildAWSResponse(response) {
    const headers = response.getHeaders ? response.getHeaders() : response.headers || {}
    headers['X-Cloud-Provider'] = 'AWS'
    return {
      statusCode: response.getStatusCode ? response.getStatusCode() : response.statusCode || 200,
      headers,
      body: Helper.buildResponseBody(response, false, this.responseAsBase64),
      isBase64Encoded: this.responseAsBase64
    }
  }

  buildAWSProxyResponse(response) {
    const headers = response.getHeaders ? response.getHeaders() : response.headers || {}
    headers['X-Cloud-Provider'] = 'AWS'
    return {
      statusCode: response.getStatusCode ? response.getStatusCode() : response.statusCode || 200,
      headers,
      multiValueHeaders: {},
      body: Helper.buildResponseBody(response, true, this.responseAsBase64),
      isBase64Encoded: this.responseAsBase64
    }
  }
}

module.exports = AwsResponseAdaptor
