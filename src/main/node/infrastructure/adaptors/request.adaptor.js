//    Copyright 2020 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

const { Logger, EnvironmentConfiguration, ObjectHelper } = require('dino-core')
const { RequestBodyParser } = require('dino-express')
const Request = require('../request')
const Helper = require('../helper')
const querystring = require('querystring')
const _ = require('lodash')

/**
 * Extends request body parser to properly extract the request body
 */
class ServerlessRequestBodyParser extends RequestBodyParser {
  extractBody(message) {
    const exposedAs = Helper.getVariable(this.environment, 'dino:cloud:expose', 'api')
    const isApi = (exposedAs || 'api').toLowerCase() === 'api'
    return Helper.ensureValue(isApi ? message.body : message, {})
  }
}

class RequestAdaptor {
  /**
   *
   * @param {EnvironmentConfiguration} environment the current environment configuration
   */
  constructor(environment) {
    this.requestBodyParser = new ServerlessRequestBodyParser(environment)
  }

  adapt(message, exposedAs) {
    if (Logger.isDebugEnabled()) {
      Logger.debug(`received new message to adapt, ${JSON.stringify(message)}`)
    }

    const url = this.buildUrl(message)
    this.setTransferEncodingIfRequired(message)
    this.headersToLowerCase(message)
    return Request.create(this.getMethod(message, exposedAs), url)
      .setHeaders(Helper.ensureValue(message.headers, {}))
      .setBody(this.requestBodyParser.parse(message))
      .setPath(Helper.ensureValue(message.path, '/'))
  }

  getMethod(message, exposedAs) {
    const _default = this.isApi(exposedAs) ? 'GET' : 'POST'
    return message.method || message.httpMethod || _default
  }

  /**
   * Build the request URL if required
   * @param {Object} message the request message
   * @returns {String}
   *
   * @private
   */
  buildUrl(message) {
    if (ObjectHelper.isDefined(message.url)) {
      return message.url
    }
    const headers = Helper.ensureValue(message.headers, {})
    const protocol = Helper.ensureValue(message.protocol, headers['X-Forwarded-Proto'] || 'http')
    const hostname = Helper.ensureValue(message.hostname, headers['Host'] || 'localhost')

    return `${protocol}://${hostname}${this.getOriginalUrl(message)}`
  }

  getOriginalUrl(message) {
    if (message.originalUrl) {
      return message.originalUrl
    }
    const query = message.multiValueQueryStringParameters || message.query || message.querystring || undefined
    const qs = query ? `?${querystring.stringify(query)}` : ''
    const originalUrl = `${Helper.ensureValue(message.resource, '/')}${qs}`
    if (!originalUrl.startsWith('/')) {
      return `/${originalUrl}`
    }
    return originalUrl
  }

  setTransferEncodingIfRequired(message) {
    if (!ObjectHelper.isDefined(message.body)) {
      return
    }
    const headers = Helper.ensureValue(message.headers, {})
    if (!headers['Transfer-Encoding'] || !headers['transfer-encoding']) {
      headers['transfer-encoding'] = 'identity'
    }
  }

  headersToLowerCase(message) {
    const headers = Helper.ensureValue(message.headers, {})
    const answer = {}
    for (const key in headers) {
      answer[key.toLowerCase()] = headers[key]
    }
    message.headers = answer
  }

  isApi(exposedAs) {
    return (exposedAs || 'api').toLowerCase() === 'api'
  }
}

module.exports = RequestAdaptor
