const Helper = require('./helper');
const OpenAPIValidator = require('express-openapi-validate').OpenApiValidator;

class ApiValidator {

  constructor(environment) {
    this.environment = environment;
    this.validationEnabled = Helper.getVariable(environment, 'dino:api:validation:enabled', true);
  }

  init(apis) {
    if(this.validationEnabled) {
      const config = this.environment.get('dino:api:validation:configuration') || { ajvOptions: { coerceTypes: true } };
      this.openApiValidator = new OpenAPIValidator(apis, config);
    }
  }

  getRequestValidator(method, path) {
    if (this.validationEnabled) {
      const validator = this.openApiValidator.validate(method, path);
      return (req, res, next) => {
        const reqToValidate = Object.assign({}, req, {
          cookies: req.cookies
            ? Object.assign({}, req.cookies, req.signedCookies) : undefined
        });
        if (!reqToValidate.hasOwnProperty('headers')) {
          reqToValidate.headers = req.headers;
        }
        return validator(reqToValidate, res, next);
      };
    }
    return (req, res, next) => next();
  }

  getResponseValidator(method, path) {
    if (this.validationEnabled) {
      const validator = this.openApiValidator.validateResponse(method, path);
      return (_req, res, next) => {
        validator(res);
        next();
      };
    }
    return () => {};
  }
}

module.exports = ApiValidator;