
//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.




class Item {
  constructor(key, value) {
    this.key = key;
    this.value = value;
    this.timestamp = Date.now();
  }

  isStillValid(maxAge) {
    return ((Date.now() - this.timestamp)/1000) < maxAge;
  }


  static create(key, value) {
    return new Item(key, value);
  }
}

class Cache {

  constructor(environment) {
    this.environment = environment;
    this.map = new Map();

    this.maxAge = this.environment.get('dino:cache:maxAge') || 300;
    this.maxItems = this.environment.get('dino:cache:maxItems') || 500;
    this.forceEvictionOnMaxItems = this.environment.get('dino:cache:forceEviction') || false;
  }


  get(key) {
    const item = this.map.get(key);
    if(item) {
      if(item.isStillValid(this.maxAge)) {
        return item.value;
      } else {
        this.evict(key);
      }
    }
    return undefined;
  }

  set(key, value) {
    if(this.map.size < this.maxItems) {
      this.map.set(key, Item.create(key, value));
    } else {
      this.__forceEvictionIfRequired(this.forceEvictionOnMaxItems);
      if (this.map.size < this.maxItems) {
        this.map.set(key, Item.create(key, value));
      }
    }
    return value;
  }

  evict(key) {
    this.map.delete(key);
  }

  __forceEvictionIfRequired(forceEviction) {
    if (forceEviction) {
      this.map.forEach(item => {
        if (!item.isStillValid(this.maxAge)) {
          this.evict(item.key);
        }
      });
    }
  }
}

module.exports = Cache;