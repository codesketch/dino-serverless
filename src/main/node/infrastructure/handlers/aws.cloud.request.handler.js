//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

'use strict'

const RequestHandler = require('./request.handler')

const Helper = require('../helper')
const { Logger } = require('dino-core')

class AwsCloudRequestHandler extends RequestHandler {
  constructor({ applicationContext, environment }) {
    super({ applicationContext, environment })
  }

  /**
   * Collect the parameters defined as part of the OpenAPI definition.
   * @param {Object} message the express request
   */
  _collectParameters(message) {
    Logger.debug('collecting parameters for AWS cloud provider')
    const parameters = Helper.ensureValue(this.api.parameters, [])
    if (!parameters.reduce) {
      return []
    }
    return parameters.reduce((acc, param) => {
      const name = param.name.replace(/-/g, '_')
      switch (param.in) {
        case 'path':
          acc[name] = Helper.ensureValue((message.params || message.pathParameters)[param.name], param.default)
          break
        case 'query':
          acc[name] = Helper.ensureValue((message.query || message.queryStringParameters)[param.name], param.default)
          break
        case 'header':
        case 'headers':
          acc[name] = Helper.ensureValue(message.headers[param.name], param.default)
          break
      }
      return acc
    }, {})
  }

  supports() {
    return 'aws'
  }
}

module.exports = AwsCloudRequestHandler
