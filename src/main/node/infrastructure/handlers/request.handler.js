//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.


const { Component, Logger, Service, Scope } = require('dino-core');
const { Interface } = require('dino-express');
const Helper = require('../helper');
const Response = require('../response');

const DEFAULT_RESPONSE = { content: { 'application/json': { schema: { type: 'object', properties: { message: { type: 'string' } } } } } };

class RequestHandler extends Component {

  constructor({ applicationContext, environment }) {
    super();
    this.environment = environment;
    this.applicationContext = applicationContext;
    this._handler = undefined;
    this.apiHandlingType = Helper.getVariable(this.environment, 'dino:api:handler:type', 'proxy');
  }

  async handle(request, response, next) {
    try {
      const handler = this.ensureHandler();
      return this.handleRequest(request, handler)
        .then(apiResponse => {
          this.normaliseSuccessResponse(apiResponse, response);
          next();
        })
        .catch(next);
    } catch (error) {
      next(error);
    }
  }

  /**
   * @returns Promise<Response|unknown>
   */
  handleRequest(message, handler) {
    Logger.debug(`handling request of type ${this.apiHandlingType}`);
    let args = message;
    if ('proxy' === this.apiHandlingType) {
      const parameters = this._collectParameters(message);
      const body = Helper.requestHasBody(message) ? message.body : undefined;
      parameters['request'] = message;
      parameters['body'] = body;
      args = parameters;
    }
    return Promise.resolve(handler[this.api.operationId](args));
  }

  addApi(api) {
    this.api = api;
    return this;
  }

  addResponseValidator(responseValidator) {
    this.responseValidator = responseValidator;
    return this;
  }

  // eslint-disable-next-line no-unused-vars
  _collectParameters(message) {
    const parameters = Helper.ensureValue(this.api.parameters, []);
    if (!parameters.reduce) {
      return [];
    }
    return parameters.reduce((acc, param) => {
      const name = param.name.replace(/-/g, '_');
      switch (param.in) {
      case 'path':
        acc[name] = Helper.ensureValue(message.params[param.name], param.default);
        break;
      case 'query':
        acc[name] = Helper.ensureValue(message.query[param.name], param.default);
        break;
      case 'header':
        acc[name] = Helper.ensureValue(message.headers[param.name], param.default);
        break;
      }
      return acc;
    }, {});
  }

  /**
   * Lookup the interface that can handle the defined operationId for the API.
   * @returns {Interface}
   * 
   * @private
   */
  ensureHandler() {
    if (!this._handler) {
      const useServiceForHandling = Helper.getVariable(this.environment, 'dino:api:handler:asService', false);
      const handlerType = useServiceForHandling ? Service : Interface;
      const candidates = this.applicationContext.resolveAll(handlerType);
      Logger.debug(`Found ${candidates.length} canditare hadlers of type ${useServiceForHandling ? 'Service' : 'Interface'}`);
      this._handler = (candidates || []).find(_interface => {
        try {
          return Reflect.has(_interface, this.api.operationId);
        } catch (e) {
          return false;
        }
      });
      if (!this._handler) {
        Logger.error(`unable to fina an interface implementing operationId ${this.api.operationId}`);
        throw Error(`unable to fina an interface implementing operationId ${this.api.operationId}`);
      }
    }
    return this._handler;
  }

  normaliseSuccessResponse(apiResponse, response) {
    const responseStatusCode = Number(apiResponse.statusCode || apiResponse.status);
    if (responseStatusCode >= 400) {
      this.normaliseErrorResponse(apiResponse, response);
      return;
    }
    // will throw ValidationError if validation fails, this is handled on the 
    // handleError method
    const statusCode = apiResponse.statusCode || Object.keys(this.api.responses).find(sc => sc.startsWith('2')) || 200;
    const definedResponse = this.api.responses[statusCode] || DEFAULT_RESPONSE;
    const contentType = apiResponse.contentType || Object.keys(definedResponse.content)[0] || 'application/json';
    const payload = apiResponse.body || apiResponse;
    const headers = apiResponse.getHeaders ? apiResponse.getHeaders() : apiResponse.headers || {};
    response.setStatusCode(statusCode).setContentType(contentType).setPayload(payload);
    Object.keys(headers).forEach(k => response.addHeader(k, headers[k]));
  }

  normaliseErrorResponse(error, response) {
    const statusCode = error.statusCode || error.status || 500;
    const definedResponse = this.api.responses[statusCode] || DEFAULT_RESPONSE;
    const contentType = Object.keys(definedResponse.content)[0];
    const properties = definedResponse.content[contentType].properties || definedResponse.content[contentType].schema.properties || { message: { type: 'string' } };
    response.setPayload(this.buildClientErrorResponse(properties, contentType, error))
      .setStatusCode(statusCode).setContentType('application/json');
  }

  buildClientErrorResponse(properties, contentType, error) {
    return Helper.format(Object.keys(properties).reduce((resp, prop) => {
      resp[prop] = error[prop] || (error['body'] || {})[prop] || '';
      return resp;
    }, {}), contentType);
  }


  static scope() {
    return Scope.TRANSIENT;
  }
}

module.exports = RequestHandler;