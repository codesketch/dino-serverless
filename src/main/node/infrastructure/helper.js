//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

'use strict'

class Helper {
  /**
   * Normalize swagger path definition to express notation
   * @param {String} path the API path
   *
   * @public
   * @static
   */
  static normalizePath(path) {
    let answer = path
    if (/.*{.*}.*/.test(path)) {
      answer = path.replace(/{/g, ':').replace(/}/g, '')
    }
    return answer
  }

  /**
   * Allows to retrieve a variable from the configuration or as environment variable, supports
   * both `:` and `_` notation.
   * @param {Environment} environment the configuration container
   * @param {string} key the key for the varible to retrieve
   * @param {any} _default the default value if the variable is nt defined.
   */
  static getVariable(environment, key, _default) {
    if (environment.getOrDefault) {
      return environment.getOrDefault(key, _default)
    }
    const envVarKey = key.replace(/:/g, '_')
    return environment.get(envVarKey) || environment.get(envVarKey.toUpperCase()) || environment.get(key) || _default
  }

  /**
   * Ensure that either the value or default value are returned.
   * @param {any} value the value to verify
   * @param {any} the default value to return
   */
  static ensureValue(value, _default) {
    return value || _default
  }

  static requestHasBody(req) {
    return ['post', 'put', 'patch'].includes(req.method.toLowerCase())
  }

  /**
   * Format the response based on the content type defined
   * @returns {String}
   */
  static format(payload, contentType) {
    if (['application/xml', 'text/xml'].some((type) => type == contentType)) {
      return require('xml')(payload)
    }
    return payload
  }

  static isAPromise(obj) {
    return (!!obj && obj instanceof Promise) || ((typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function')
  }

  /**
   * Evaluate if an obj is an instance of a type
   *
   * @param {Any} obj the object to be verified
   * @param {Any} type the type the object will be verified against
   * @returns {Boolean} true if obj is instance of type, fasle otherwise
   */
  static instanceOf(obj, type) {
    // eslint-disable-next-line no-prototype-builtins
    return (
      type.isPrototypeOf(obj) ||
      obj instanceof type ||
      // eslint-disable-next-line no-prototype-builtins
      (obj.hasOwnProperty('prototype') && obj.prototype instanceof type) ||
      // eslint-disable-next-line no-prototype-builtins
      obj.isPrototypeOf(type)
    )
  }

  static base64RegExp = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/  ///^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$/gi
  static isBase64(v) {
    if (typeof v === 'string' || v instanceof String) {
      if (v.trim().length === 0 || v.length % 4 !== 0) {
        return false
      }
      return Helper.base64RegExp.test(v)
    }
    return false
  }

  static decodeFromBase64AndParse(str) {
    if (Helper.isBase64(str)) {
      return JSON.parse(Buffer.from(str, 'base64').toString())
    } else {
      return str
    }
  }

  /**
   * Normalise the response body from the received error
   * @param {any} response the response for the client
   * @returns the body response
   *
   * @private
   */
  static buildResponseBody(response, asString, asBase64) {
    let body = response.getBody ? response.getBody() : response.body
    if (!body && response.data) {
      body = {
        name: response.name,
        data: response.data
      }
    }
    if (asBase64) {
      return Buffer.from(JSON.stringify(body || {}), 'utf-8').toString('base64')
    }
    return asString ? JSON.stringify(body || {}) : body || {}
  }
}

module.exports = Helper
