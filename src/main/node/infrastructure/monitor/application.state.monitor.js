const ApplicationStateMonitor = require('dino-express/src/main/node/monitoring/application.state.monitor')
const Response = require('../response')

module.exports = class ServerlessApplicationStateMonitor extends ApplicationStateMonitor {
  doMonitor(res) {
    let monitors = this.getApplicationStateMonitors()
    let properties = { default: [{ name: 'status', value: 'UP' }] }
    if (monitors && monitors.length > 0) {
      properties = monitors.reduce((answer, monitor) => {
        let state = monitor.execute()
        answer[monitor.name()] = Array.from(state.getProperties()).map((entry) => {
          return { name: entry[0], value: entry[1] }
        })
        return answer
      }, properties)
    }
    return Response.build(properties).setContentType('application/json').setStatusCode(200)
  }

  middleware() {
    // eslint-disable-next-line no-unused-vars
    return (req, response, next) => {
      const apiResponse = this.doMonitor.bind(this)(response)
      const responseStatusCode = Number(apiResponse.statusCode || apiResponse.status)
      if (responseStatusCode >= 400) {
        this.normaliseErrorResponse(apiResponse, response)
        return
      }
      // will throw ValidationError if validation fails, this is handled on the
      // handleError method
      const statusCode = apiResponse.statusCode || Object.keys(this.api.responses).find((sc) => sc.startsWith('2')) || 200
      const contentType = apiResponse.contentType || 'application/json'
      const payload = apiResponse.body || apiResponse
      const headers = apiResponse.getHeaders ? apiResponse.getHeaders() : apiResponse.headers || {}
      response.setStatusCode(statusCode).setContentType(contentType).setPayload(payload)
      Object.keys(headers).forEach((k) => response.addHeader(k, headers[k]))
      next()
    }
  }
}
