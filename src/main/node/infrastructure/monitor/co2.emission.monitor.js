//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

const CO2EmissionMonitor = require('dino-express/src/main/node/monitoring/co2.emission.monitor')

class ServerlessCO2EmissionMonitor extends CO2EmissionMonitor {
  _collectRequestInfoRequired(req) {
    const requests = this.properties.get('requests')
    const bytes = req.socket && req.socket.bytesRead ? req.socket.bytesRead : JSON.stringify(req).length
    const co2Emission = this.co2EmissionCalculator.perByte(bytes)
    requests.total += 1
    requests.totalCO2GramsPerByteRead += co2Emission
    this.properties.set('totalCO2GramsPerByte', this.properties.get('totalCO2GramsPerByte') + co2Emission)
  }

  _collectResponseInfoRequired(res) {
    const responses = this.properties.get('responses')
    const bytes = res.socket && res.socket.bytesWritten ? res.socket.bytesWritten : JSON.stringify(res).length
    const co2Emission = this.co2EmissionCalculator.perByte(bytes)
    responses.total += 1
    responses.totalCO2GramsPerByteWritten += co2Emission
    this.properties.set('totalCO2GramsPerByte', this.properties.get('totalCO2GramsPerByte') + co2Emission)
  }
}

module.exports = ServerlessCO2EmissionMonitor
