//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

'use strict'

const Helper = require('../helper')

class MixedDescriptorProvider {
  constructor(environment) {
    this.environment = environment
  }

  /**
   * Provide a reference to an OpenAPI descriptor stored on the local file-system, the location
   * of the descriptor can be provided using any of the `dino:openapi:descriptor`,
   * `dino_openapi_descriptor` or `DINO_OPENAPI_DESCRIPTOR` configuration variables,
   * if the configuration is not provided the default `src/main/resources/routes.yml` path will
   * be used.
   * @returns {string | object} the path of the OpenAPI descriptor file or a default events descriptor.
   */
  provide() {
    const definedDescriptor = Helper.getVariable(this.environment, 'dino:openapi:descriptor', undefined)
    if (!definedDescriptor) {
      return this.getDefaultEventDescriptor()
    }
    return definedDescriptor
  }

  getDefaultEventDescriptor() {
    return {
      provide: () => ({
        openapi: '3.0.1',
        info: {
          version: '0.0.1',
          title: 'DinO Serverless',
          license: {
            name: 'Apache 2'
          },
          description: 'DinO Serverless Event Handler'
        },
        servers: [
          {
            url: 'http://localhost:3031'
          }
        ],
        paths: {
          '/': {
            post: {
              summary: 'Receives an event',
              description: 'An API that allows to receive envents',
              operationId: 'handle',
              tags: ['api'],
              parameters: [],
              requestBody: {
                description: 'the message to receive',
                content: {
                  'application/json': {
                    schema: {
                      $ref: '#/components/schemas/Event'
                    }
                  }
                }
              },
              responses: {
                201: {
                  description: 'Generic response',
                  content: {
                    'application/json': {
                      schema: {
                        $ref: '#/components/schemas/GenericResponse'
                      }
                    }
                  }
                },
                default: {
                  description: 'unexpected error',
                  content: {
                    'application/json': {
                      schema: {
                        $ref: '#/components/schemas/Error'
                      }
                    }
                  }
                }
              }
            }
          }
        },
        components: {
          schemas: {
            Event: {
              type: 'object',
              additionalProperties: true
            },
            GenericResponse: {
              type: 'object',
              additionalProperties: true
            },
            Error: {
              type: 'object',
              additionalProperties: true
            }
          }
        }
      })
    }
  }
}

module.exports = MixedDescriptorProvider
