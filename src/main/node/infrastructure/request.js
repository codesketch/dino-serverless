/* eslint-disable no-unused-vars */

//    Copyright 2020 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

const parseurl = require('parseurl')
const queryString = require('query-string')

class Request {
  constructor(method, url) {
    this.url = url
    this.method = method
    this.body = undefined
    this.query = queryString.parseUrl(this.url).query
    this.params = {}
    this.headers = {}
    this.requestPath = undefined
  }

  path(exposedAs) {
    return this.requestPath || parseurl({ url: this.url }).pathname
  }

  getMethod(exposedAs) {
    return this.method
  }

  setPath(path) {
    this.requestPath = path
    return this
  }

  setHeaders(headers = {}) {
    this.headers = headers
    return this
  }

  setBody(body) {
    this.body = body
    return this
  }

  resolveParams(keys, match) {
    if (match === undefined) {
      return
    }
    for (var i = 1; i < match.length; i++) {
      var key = keys[i - 1]
      var prop = key.name
      var val = this.__decodeParam(match[i])
      if (val !== undefined || !hasOwnProperty.call(this.params, prop)) {
        this.params[prop] = val
      }
    }
  }

  isGet() {
    return (this.method || '').toLowerCase() == 'get'
  }

  static create(method, url) {
    return new Request(method, url)
  }

  // private method
  __decodeParam(val) {
    if (typeof val !== 'string' || val.length === 0) {
      return val
    }

    try {
      return decodeURIComponent(val)
    } catch (err) {
      if (err instanceof URIError) {
        err.message = "Failed to decode param '" + val + "'"
        err.status = err.statusCode = 400
      }

      throw err
    }
  }
}

module.exports = Request
