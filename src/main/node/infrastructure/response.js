//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

class Response extends require('dino-express').Response {
  constructor() {
    super()
    this.context = {}
    this.error = false
  }

  defineContext(onSuccess, onFailure, responseAdaptor, serverResponse) {
    this.context = {
      onSuccess,
      onFailure,
      responseAdaptor,
      serverResponse
    }
    return this
  }

  defineAsErrorResponse() {
    this.error = true
    return this
  }

  getContext() {
    return this.context
  }

  isErrorResponse() {
    return this.error
  }

  setPayload(payload) {
    this.body = payload
    return this
  }

  setStatusCode(statusCode) {
    this.status = this.statusCode = statusCode
    return this
  }

  status(statusCode) {
    this.statusCode = statusCode
    return this
  }

  send(body) {
    this.body = body
    return this
  }

  getStatusCode() {
    return this.statusCode
  }

  getHeaders() {
    const headers = Object.assign({}, this.headers || {}, { 'X-Powerd-By': 'dino-serverless' })
    headers['content-type'] = headers['content-type'] || headers['Content-Type'] || this.contentType
    return headers
  }

  getBody() {
    return this.body || {}
  }

  format(obj) {
    // TODO implement has per
    const fn = this.getAdaptor(obj)
    if (fn) {
      fn()
    }
    return this
  }

  getAdaptor(obj) {
    if (this.contentType === 'application/json') {
      return obj.json
    }
    return obj.default
  }
}

module.exports = Response
