//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

'use strict'

const Router = require('./routing/router')
const RequestHandlerFactory = require('./request.handler.factory')
const Helper = require('./helper')
const { Logger, ComponentDescriptor, Scope, Resolver } = require('dino-core')

const { RoutingConfigurer, AfterHandlerMiddleware, BeforeHandlerMiddleware } = require('dino-express')
const ApiValidator = require('./api.validator')
const ResponseMiddleware = require('./middlewares/response.middleware')
const EventDescriptorProvider = require('./providers/event.descriptor.provider')
const MixedDescriptorProvider = require('./providers/mixed.descriptor.provider')
const ServerlessCO2EmissionMonitor = require('./monitor/co2.emission.monitor')
const ServerlessApplicationStateMonitor = require('./monitor/application.state.monitor')

class RoutingConfigurerServerless extends RoutingConfigurer {
  constructor({ applicationContext, environment }) {
    super({ applicationContext, environment })
    this.requestHandlerFactory = new RequestHandlerFactory(applicationContext)
  }

  async postConstruct() {
    const serverType = Helper.getVariable(this.environment, 'dino:server:type', 'serverless')
    Logger.debug(`loading configuration for ${serverType} server type`)
    if (serverType === 'express') {
      // we want to use express... maybe for development purposes
      return await super.postConstruct()
    } else {
      const router = new Router()
      this.applicationContext.register('router', ComponentDescriptor.create('router', router, Scope.SINGLETON, Resolver.VALUE, false, []))
      this.extend(router)

      const cloudProvider = Helper.getVariable(this.environment, 'dino:cloud:provider', 'google')
      Logger.info(`running in a ${cloudProvider} serverless infrastructure`)

      const swaggerParser = this.applicationContext.require('swagger-parser')
      const apis = await swaggerParser.dereference(this.resolveDescriptorProvider().provide())
      const apiValidator = new ApiValidator(this.environment)
      apiValidator.init(apis)
      const errorHandler = this.errorHandler(cloudProvider)
      this.setupMonitoringEndpointsIfRequired(router)
      const monitoringMiddlewares = this.defineTopLevelMonitorsIfRequired()

      // map defined OpenAPI APIs
      Object.keys(apis.paths).forEach((path) => {
        const currentPath = apis.paths[path]
        Object.keys(currentPath).forEach((method) => {
          const apiPath = Helper.normalizePath(path)
          const api = currentPath[method]
          const responseValidator = apiValidator.getResponseValidator(method, path)
          const requestHandler = this.requestHandler(api, responseValidator, cloudProvider)

          // defines middlewares proposing built-ins and concatenating eser ones
          // as per express style the user will be able to interrupt the chain just
          // sending a response, this at current stage is not a concern but a better
          // middleware structure that avoid this kind if hacks should be put in place.
          // this is tracked on https://gitlab.com/codesketch/dino-express/issues/7
          const userDefinedMiddlewares = this.middlewares(api, apis.components) || []
          let middlewares = monitoringMiddlewares.before.concat([apiValidator.getRequestValidator(method, path)])
          // set BeforeHandlerMiddleware or unknown-type middlewares
          middlewares = middlewares
            .concat(monitoringMiddlewares.after)
            .concat(
              userDefinedMiddlewares
                .filter((middleware) => !Helper.instanceOf(middleware, AfterHandlerMiddleware))
                .map((middleware) =>
                  Helper.instanceOf(middleware, BeforeHandlerMiddleware) ? middleware.handle.bind(middleware) : middleware
                )
            )
          middlewares = middlewares.concat([requestHandler, errorHandler])
          // set AfterHandlerMiddleware middlewares
          middlewares = middlewares.concat(
            userDefinedMiddlewares
              .filter((middleware) => Helper.instanceOf(middleware, AfterHandlerMiddleware))
              .map((middleware) =>
                Helper.instanceOf(middleware, AfterHandlerMiddleware) ? middleware.handle.bind(middleware) : middleware
              )
          )
          middlewares = middlewares.concat([apiValidator.getResponseValidator(method, path), ResponseMiddleware.handler])
          router.addApi(method, apiPath, api.operationId, middlewares, undefined)
        })
      })
      Logger.info('APIs definition loaded')
    }
  }

  setupMonitoringEndpointsIfRequired(router) {
    let monitoringEnabled = this.getVariable('dino:server:monitoring:enabled', true)
    if (monitoringEnabled) {
      this.applicationStateMonitor = new ServerlessApplicationStateMonitor(this.applicationContext)
      if(router.addApi) {
        router.addApi('GET', '/monitor', '', [this.applicationStateMonitor.middleware(this.applicationContext), ResponseMiddleware.handler])
      } else {
        super.setupMonitoringEndpointsIfRequired(router)
      }
    }
  }

  /**
   * Allows to define top level monitors. The returned obects contains two arrays
   * before and after middlewares, the before middlewares are added at the very start
   * of the middleware chain while the after are added at the very end of the middleware
   * chain, both in the order that are defined.
   *
   * @returns {Object}
   * ```{
   *    before: [],
   *    after: []
   *  }```
   */
  defineTopLevelMonitorsIfRequired() {
    const answer = { before: [], after: [] }
    let monitoringEnabled = this.getVariable('dino:server:monitoring:enabled', true)
    let co2MonitoringEnabled = this.getVariable('dino:server:monitoring:co2', true)
    if (monitoringEnabled) {
      if (co2MonitoringEnabled) {
        const co2Monitor = new ServerlessCO2EmissionMonitor()
        this.addMonitor(co2Monitor)
        answer.before.push(co2Monitor.requestMiddleware.bind(co2Monitor))
        answer.after.push(co2Monitor.responseMiddleware.bind(co2Monitor))
      }
    }
    return answer
  }

  resolveDescriptorProvider() {
    const exposedAs = Helper.getVariable(this.environment, 'dino:cloud:expose', 'api')
    if (exposedAs.toLowerCase() === 'api') {
      return super.resolveDescriptorProvider()
    } else if (exposedAs.toLowerCase() === 'mixed') {
      return new MixedDescriptorProvider(this.environment)
    } else if ('event') {
      return new EventDescriptorProvider()
    }
    throw new Error('unknown service mode, please configure dino:cloud:expose property as api, mixed or event')
  }

  requestHandler(api, responseValidator, cloudProvider) {
    const serverType = Helper.getVariable(this.environment, 'dino:server:type', 'serverless')
    if ('express' === serverType) {
      return super.requestHandler(api, responseValidator)
    }
    const handler = this.requestHandlerFactory.get(cloudProvider)
    // const handler = new RequestHandler({ applicationContext: this.applicationContext, environment: this.environment });
    handler.addApi(api)
    return handler.handle.bind(handler)
  }

  errorHandler(cloudProvider) {
    if (cloudProvider === 'aws') {
      // AWS doesn't have a format method on response
      // eslint-disable-next-line no-unused-vars
      return (err, req, res, next) => {
        if (!err.status && err.statusCode) {
          err.status = err.statusCode
        }

        const body = {
          status: err.status || err.statusCode,
          message: err.message
        }
        if (err.status >= 400 && err.status <= 499) {
          ;['code', 'name', 'type', 'details'].forEach(function (prop) {
            if (err[prop]) body[prop] = err[prop]
          })
        }
        res
          .setPayload(body)
          .setStatusCode(err.statusCode || 500)
          .setContentType('application/json')
        next()
      }
    } else {
      return super.errorHandler()
    }
  }
}

module.exports = RoutingConfigurerServerless
