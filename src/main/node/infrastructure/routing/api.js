//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

'use strict'

const { Logger } = require('dino-core')
const { pathToRegexp } = require('path-to-regexp')
// eslint-disable-next-line no-unused-vars
const Request = require('../request')
// eslint-disable-next-line no-unused-vars
const Response = require('../response')

class Api {
  constructor(method, path, operationId, middlewares, responseValidator) {
    this.keys = []
    this.path = path
    this.method = method
    this.operationId = operationId
    this.middlewares = []
    this.errorHandlers = []
    this.responseValidator = responseValidator

    // @see https://github.com/expressjs/express/blob/master/lib/router/layer.js
    this.regexp = pathToRegexp(path, this.keys, {})
    // set fast path flags
    this.regexp.fast_star = path === '*'
    this.regexp.fast_slash = path === '/'

    this._selectMiddlewares(middlewares)
  }

  /**
   * Check if this route matches `path`, if so
   * populate `.params`.
   *
   * @param {String} path
   * @return {Boolean}
   * @api public
   */
  matches(currentPath) {
    if (currentPath != null) {
      // fast path non-ending match for / (any path matches)
      if (this.regexp.fast_slash) {
        return true
      }
      // fast path for * (everything matched in a param)
      if (this.regexp.fast_star) {
        return true
      }
      // match the path
      this.pathMatch = this.regexp.exec(currentPath)
      return this.pathMatch
    }
    return false
  }

  // eslint-disable-next-line no-unused-vars
  validateResponse(response) {
    return true //this.responseValidator(response);
  }

  /**
   * @param {Request} req
   * @param {Response} res
   */
  process(request, response, cloudProvider) {
    let mIndex = 0
    let eIndex = 0
    const handler = (error) => {
      if (error) {
        Logger.error(`unable to complete request ${error.message}`)
        if (eIndex < this.errorHandlers.length) {
          response.defineAsErrorResponse()
          // GCP is based on express, propagate the response as so that we can use express structure
          const res = ['google', 'gcp'].includes(cloudProvider) ? response.getContext().serverResponse : response
          this.errorHandlers[eIndex++](error, request, res, handler.bind(this))
        }
      } else {
        if (mIndex < this.middlewares.length) {
          this.middlewares[mIndex++](request, response, handler.bind(this))
        }
      }
    }
    request.resolveParams(this.keys, this.pathMatch)
    handler.bind(this)()
  }

  // private methods
  _selectMiddlewares(middlewares) {
    for (let index = 0; index < middlewares.length; index++) {
      const middleware = middlewares[index]
      if (middleware.length <= 3) {
        this.middlewares.push(middleware)
      } else if (middleware.length == 4) {
        this.errorHandlers.push(middleware)
      }
    }
  }

  static create(method, path, operationId, middlewares, responseValidator) {
    return new Api(method, path, operationId, middlewares, responseValidator)
  }
}

module.exports = Api
