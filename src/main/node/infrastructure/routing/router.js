//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.




'use strict';

const Api = require('./api');

class Router {

  constructor() {
    this.apis = new Map();
    this.middlewares = new Array();
  }

  use(middleware) {
    this.middlewares.push(middleware);
  }

  addApi(method, path, operationId, middlewares, responseValidator) {
    const apis = this.apis.get(method.toLowerCase()) || [];
    apis.push(Api.create(method, path, operationId, this.middlewares.concat(middlewares), responseValidator));
    this.apis.set(method.toLowerCase(), apis);
  }

  /**
   * Get the handling API definition
   * @param {String} method the HTTP API method
   * @param {String} path the current invoke path
   * 
   * @public
   */
  getApi(method, path) {
    return (this.apis.get(method.toLowerCase()) || []).find(api => api.matches(path));
  }

}

module.exports = Router;