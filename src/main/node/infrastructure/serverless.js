//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global setTimeout */
/* global setInterval */
/* global clearInterval */

const Helper = require('./helper')
const RequestAdaptor = require('./adaptors/request.adaptor')
const { Dino, Logger } = require('dino-core')
const Response = require('./response')
const ResponseAdaptor = require('./adaptors/response.adaptor')

let _instance = undefined
class Serverless {
  constructor() {
    this.router = undefined
    this.applicationContext = undefined
    this.requestAdaptor = undefined
    this.responseAdaptor = undefined
  }

  async loadContext(config) {
    this.applicationContext = await Dino.run(config)
    this.environment = this.applicationContext.resolve('environment')
    this.requestAdaptor = new RequestAdaptor(this.environment)
    this.responseAdaptor = new ResponseAdaptor(this.environment)
    this.serverType = Helper.getVariable(this.environment, 'dino:server:type', 'serverless')
    this.cloudProvider = Helper.getVariable(this.environment, 'dino:cloud:provider', 'google')
    this.exposedAs = Helper.getVariable(this.environment, 'dino:cloud:expose', 'api')
    this.intervalTimer = setInterval(this.__discoverRouter.bind(this), 100)
    return this
  }

  static getInstance() {
    if (!_instance) {
      _instance = new Serverless()
      _instance.loadContext()
    }
    return _instance
  }

  handler(request, response, reject) {
    const internalRequest = this.requestAdaptor.adapt(request, this.exposedAs)
    const method = internalRequest.getMethod(this.exposedAs)
    const path = internalRequest.path(this.exposedAs)
    Logger.debug(`Looking for defined API with method: ${method} and path ${path}`)
    const api = this.router.getApi(method, path)
    if (!api) {
      Logger.error(
        `API definition not found for method: ${method} and path ${path} did you define the API as part of the OpenAPI definition?`
      )
      return this.__sendResponse(this.__buildErrorResponse({ message: 'unable to find an api for the requested path' }), response, reject)
    }
    return api.process(internalRequest, response, this.cloudProvider)
  }

  expose(request, response) {
    if (this.serverType === 'express') {
      return Promise.resolve()
    }
    return new Promise((resolve, reject) => {
      let res = new Response().defineContext(resolve, reject, this.responseAdaptor, response)
      if (this.router) {
        Logger.debug('router has already been initialized...')
        return this.handler(request, res, reject)
      } else {
        Logger.info('router not found initialize it...')
        const resolver = () => {
          this.router = this.applicationContext.resolve('router')
          clearInterval(this.intervalTimer)
          return this.handler.bind(this)(request, res, reject)
        }
        return setTimeout(resolver.bind(this), 1000)
      }
    })
  }

  // private

  __discoverRouter() {
    try {
      this.router = this.applicationContext.resolve('router')
      clearInterval(this.intervalTimer)
    } catch (error) {
      // nothing to do
    }
  }

  __buildErrorResponse(payload) {
    if (Helper.instanceOf(payload, Response)) {
      return payload
    }
    return Response.build({ message: payload.message }).setStatusCode(500).setContentType('application/json')
  }

  __sendResponse(currentResponse, response, callback, isError = false) {
    return this.responseAdaptor.adapt(currentResponse, response, callback, isError)
  }
}

module.exports = Serverless
