const { describe, it } = require('mocha')
const RequestAdaptor = require('../../../../../main/node/infrastructure/adaptors/request.adaptor')
const { expect } = require('chai')

describe('request adaptor', () => {
  it('decode body if isBase64Encoded is set as part of the message', () => {
    // prepare
    const testObj = new RequestAdaptor({
      get: (key) => {
        if ('dino:server:request:forceBase64Decode:enabled' === key) {
          return false
        }
        if ('dino:server:request:forceBase64Decode:paths' === key) {
          return []
        }
        return undefined
      }
    })
    const message = {
      isBase64Encoded: true,
      body: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ=='
    }
    // act
    const actual = testObj.adapt(message, 'api')
    // assert
    expect(actual.body).to.be.eqls({ params: { message: 'abc' } })
  })

  it('decode body untouched if isBase64Encoded and forceBase64RequestDecode are false', () => {
    // prepare
    const testObj = new RequestAdaptor({
      get: (key) => {
        if ('dino:server:request:forceBase64Decode:enabled' === key) {
          return false
        }
        if ('dino:server:request:forceBase64Decode:paths' === key) {
          return []
        }
        return undefined
      }
    })
    const message = {
      isBase64Encoded: false,
      body: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ=='
    }
    // act
    const actual = testObj.adapt(message, 'api')
    // assert
    expect(actual.body).to.be.eqls('eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==')
  })

  it('decode body if encoded and isforceBase64RequestDecode is true', () => {
    // prepare
    const testObj = new RequestAdaptor({
      get: (key) => {
        if ('dino:server:request:forceBase64Decode:enabled' === key) {
          return true
        }
        if ('dino:server:request:forceBase64Decode:paths' === key) {
          return []
        }
        return undefined
      }
    })
    const message = {
      isBase64Encoded: false,
      body: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ=='
    }
    // act
    const actual = testObj.adapt(message, 'api')
    // assert
    expect(actual.body).to.be.eqls({ params: { message: 'abc' } })
  })

  it('decode top level properties if forceBase64RequestDecode is true and paths are not defined', () => {
    // prepare
    const testObj = new RequestAdaptor({
      get: (key) => {
        if ('dino:server:request:forceBase64Decode:enabled' === key) {
          return true
        }
        if ('dino:server:request:forceBase64Decode:paths' === key) {
          return []
        }
        return undefined
      }
    })
    const message = {
      isBase64Encoded: false,
      body: { data: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==' }
    }
    // act
    const actual = testObj.adapt(message, 'api')
    // assert
    expect(actual.body).to.be.eqls({ data: { params: { message: 'abc' } } })
  })

  it('decode path properties if forceBase64RequestDecode is true and paths are defined', () => {
    // prepare
    const testObj = new RequestAdaptor({
      get: (key) => {
        if ('dino:server:request:forceBase64Decode:enabled' === key) {
          return true
        }
        if ('dino:server:request:forceBase64Decode:paths' === key) {
          return ['data']
        }
        return undefined
      }
    })
    const message = {
      isBase64Encoded: false,
      body: { data: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==', info: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==' }
    }
    // act
    const actual = testObj.adapt(message, 'api')
    // assert
    expect(actual.body).to.be.eqls({ data: { params: { message: 'abc' } }, info: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==' })
  })

  it('decode path properties if forceBase64RequestDecode is true and multiple paths are defined', () => {
    // prepare
    const testObj = new RequestAdaptor({
      get: (key) => {
        if ('dino:server:request:forceBase64Decode:enabled' === key) {
          return true
        }
        if ('dino:server:request:forceBase64Decode:paths' === key) {
          return ['data', 'info']
        }
        return undefined
      }
    })
    const message = {
      isBase64Encoded: false,
      body: { data: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==', info: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==' }
    }
    // act
    const actual = testObj.adapt(message, 'api')
    // assert
    expect(actual.body).to.be.eqls({ data: { params: { message: 'abc' } }, info: { params: { message: 'abc' } } })
  })

  it('decode path properties if forceBase64RequestDecode is true and multiple nested paths are defined', () => {
    // prepare
    const testObj = new RequestAdaptor({
      get: (key) => {
        if ('dino:server:request:forceBase64Decode:enabled' === key) {
          return true
        }
        if ('dino:server:request:forceBase64Decode:paths' === key) {
          return ['data', 'info.encoded']
        }
        return undefined
      }
    })
    const message = {
      isBase64Encoded: false,
      body: { data: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==', info: { encoded: 'eyJwYXJhbXMiOiB7ICJtZXNzYWdlIjogImFiYyJ9fQ==' } }
    }
    // act
    const actual = testObj.adapt(message, 'api')
    // assert
    expect(actual.body).to.be.eqls({ data: { params: { message: 'abc' } }, info: { encoded: { params: { message: 'abc' } } } })
  })
})
